<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <p>Seleccione una marca</p>
                 <?php
                //Consulta sin parámetros
                $sel = $con->prepare("SELECT *from marca");
                $sel->execute();
                $res = $sel->get_result();?>               
                <select>
                    <option value="0">Marcas:</option> 
                <?php while ($f = $res->fetch_assoc()) { ?> 
                    <?php
                echo '<option value="'.$f['marca_id'].'">'.$f['marca_nombre'].'</option>';
                 ?>
                 <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                </select>                             
            </div>
        </div>
    </body>
</html>