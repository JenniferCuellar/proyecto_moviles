<!– PARA EJEMPLO DASC — >
<!DOCTYPE html>
<html>
    <head>
        <title>Agregar Sucursal</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php 
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        ?>
       
    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">            
                <h1>Registrar una Sucursal</h1>
                <form role="form" id="login-form" 
                      method="post" class="form-signin" 
                      action="sucursal_guardar.php">
                <p> Seleccione un proveedor</p>
                 <?php
                //Consulta sin parámetros
                $sel = $con->prepare("SELECT *from proveedor");
                $sel->execute();
                $res = $sel->get_result();?>               
                <select name="proveedor_id" id="proveedor_id">
                    
                <?php while ($f = $res->fetch_assoc()) { ?> 
                    <?php
                echo '<option value="'.$f['proveedor_id'].'">'.$f['proveedor_nombre'].'</option>';
                 ?>
                 <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                </select>       

                    <div class="h2">
                        DATOS DE LA SUCURSAL
                    </div>


                    <div class="form-group">
                        <label for="sucursal_nombre">Nombre Sucursal (requerido)</label>
                        <input type="text" class="form-control" id="sucursal_nombre" name="sucursal_nombre"
                               placeholder="Ingresa nombre de Sucursal" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Direcci&oacute;n</label>
                        <input type="text" class="form-control" id="sucursal_direccion" name="sucursal_direccion"
                               placeholder="Ingresa direcci&oacute;n" style="text-transform:uppercase;">
                    </div>

                    <div class="form-group">
                        <label>Tel&eacute;fono 1</label>
                        <input type="tel" class="form-control" id="sucursal_telefono" name="sucursal_telefono"
                               placeholder="Ingresa primer tel&eacute;fono" style="text-transform:uppercase;">
                    </div>

                    <label>Tel&eacute;fono 2</label>
                    <input type="tel" class="form-control" id="sucursal_telefono_2" name="sucursal_telefono_2"
                           placeholder="Ingresa segundo tel&eacute;fono" style="text-transform:uppercase;">
                    <br>
                    <div class="form-group">
                        <label for="sucursal_correo">Correo electr&oacute;nico</label>

                        <input type="email" class="form-control" id="sucursal_correo" name="sucursal_correo"
                               placeholder="Ingresa correo electr&oacute;nico" style="text-transform:uppercase;">

                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>

    </body>
</html>

