<!DOCTYPE html>
<html>
    <head>
        <title>Seleccionar una refacción para agregar una nueva cotización</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <?php
                //Consulta sin parámetros
                $sel = $con->prepare("SELECT *from refaccion");

                /* consulta con parametros
                  $sel = $con->prepare("SELECT *from marca WHERE marca_id<=?");
                  $parametro = 50;
                  $sel->bind_param('i', $parametro);
                  finaliza consulta con parámetros */

                $sel->execute();
                $res = $sel->get_result();
                ?>
               <div class="h2">
                    Agregar cotizaciones
                </div>
                <div class="h3">
                    Para realizar la cotización eliga una refacci&oacute;n
                </div>
                <div class="h4">
                   1.- Selecciona la refacci&oacute;n
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <th>ID REFACCI&oacute;n</th>
                    <th>NOMBRE REFACCI&oacute;n</th>
                    </thead>
                    <tfoot>
                    <th>ID REFACCI&oacute;n</th>
                    <th>NOMBRE REFACCI&oacute;n</th>
                    </tfoot>
                    <tbody>
                      <?php while ($f = $res->fetch_assoc()) { ?>
                            <tr>
                                <td>
                                    <?php echo $f['refaccion_id'] ?>
                                </td>
                                <td>
        <a href="cotizacion_agregar.php?
        refaccion_id=<?php echo $f['refaccion_id']?>
        & refaccion_nombre=<?php echo $f['refaccion_nombre'] ?>">
            <?php echo $f['refaccion_nombre']?></a>
                                </td>
                            </tr>
                            <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                    <tbody>
                </table>
            </div>
        </div>
        <?php
        include 'inc/incluye_datatable_pie.php';
        ?>
    </body>
</html>

