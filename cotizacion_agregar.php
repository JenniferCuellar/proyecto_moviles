<!– PARA EJEMPLO DASC — >
<?php
$id_refaccion_seleccionada = $_GET['refaccion_id'];
$nombre_refaccion_seleccionada = $_GET['refaccion_nombre'];
?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php 
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        ?>
        <!--termina código que incluye Bootstrap-->

    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <form enctype="multipart/form-data" role="form" id="login-form" method="post" class="form-signin" action="cotizacion_guardar.php">
                    <div class="h2">
                        Cotizar con el proveedor
                    </div>

                    <div class="form-group">
                        <label>ID de la refacción seleccionada (<?php echo $nombre_refaccion_seleccionada ?>)</label>
                        <input type="text" id="id_refaccion" class="form-control" name="id_refaccion" value="<?php echo $id_refaccion_seleccionada ?>" readonly="" 
                               placeholder="<?php echo $nombre_refaccion_seleccionada ?>">
                    </div>

                    <div class="form-group">
                        <label>Selecciona el proveedor con el que estas cotizando (requerido)</label>
                        <?php
                //Consulta sin parámetros
                $sel = $con->prepare("SELECT *from proveedor");
                $sel->execute();
                $res = $sel->get_result();?>               
                <select name="proveedor_id" id="proveedor_id">
                    
                <?php while ($f = $res->fetch_assoc()) { ?> 
                    <?php
                echo '<option value="'.$f['id_proveedor'].'">'.$f['proveedor_nombre'].'</option>';
                 ?>
                 <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                </select>           
                    </div>
                    
                    <div class="form-group">
                        <label>Fecha de solicitud de precio (requerido)</label>
                        <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" step="1"
                               value="<?php echo date("Y-m-d");?> required>
                    </div>
                    <div class="form-group">
                        <label>Precio $ (requerido)</label>
                        <input type="number" class="form-control" id="precio" name="precio" step="0.01" placeholder="Precio actual">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
    </body>
</html>